# CNV calling 

- Installation: Go to Number [#1](#1-installation)
- Usage [#2](#2-usage):
	- Complete pipeline in batch [#2.1](#21-complete-pipeline-in-batch)
        - Batch 1 [#2.1.1](#211-batch-1)
        - Batch 2 [#2.1.2](#212-batch-2)
	- Step-by-step analysis [#2.2](#22-step-by-step-analysis):
        - Obtain gender of the samples [#2.1.1](#221-obtain-gender-of-the-samples)
        - DECoN [#2.1.2](#222-decon)
        - CNVkit [#2.1.3](#223-cnvkit)
        - panelcn.MOPS [#2.1.4](#224-panelcnmops)
        - Merge individual call sets [#2.1.5](#225-merge-individual-call-sets)
        - Optimization [#2.1.6](#226-optimization)
        - Annotation [#2.1.7](#227-annotation)

## 1.- Installation

### 1.1.- Pre-requisites

The following softwares have to be properly installed:
- Python 3.5 or later and its following packages:
	- [Pandas](http://pandas.pydata.org)
	- [Biopython](http://biopython.org/wiki/Main_Page)
	- [pybedtools](https://daler.github.io/pybedtools/index.html)
	- [sourcedefender](https://pypi.org/project/sourcedefender/)
	- [concurrent](https://docs.python.org/3/library/concurrent.html)
- BEDtools from https://github.com/arq5x/bedtools2
- DECoN from https://github.com/RahmanTeam/DECoN
- CNVkit from https://github.com/etal/cnvkit
- panelcn.MOPS from https://github.com/bioinf-jku/panelcn.mops
- optimizer from CNVbenchmarkeR from https://github.com/TranslationalBioinformaticsIGTP/CNVbenchmarkeR
- AnnotSV from https://github.com/lgmgeo/AnnotSV
- sambamba (version >= 0.8.0) from https://github.com/biod/sambamba

### 1.2.- Installation

1. Download repository
```shell
git clone git@gitlab.com:sequentiateampublic/isocnv
```

## 2.- Usage

### 2.1.- Complete pipeline in batch

There is two version:
- [Batch 1](#211-batch-1): Perform complete WES or TGS CNV calling. DECoN algorithm is applied separately between male and female samples
- [Batch 2](#211-batch-1): Perform complete WES or TGS CNV calling. DECoN algorithm is applied separately between male and female samples only for sexual chromosomes.

#### 2.1.1.- Batch 1

##### 2.1.1.1. - Usage
```python
usage: isoCNV.py batch [-h] -f FASTA [-o FILENAME] [-O DIRECTORY] [-T TOOL]
                        [-g GENOME] [-pl] [-rS] [-rR] [-pD DIRECTORY]
                        [-pO DIRECTORY] [-pA DIRECTORY] [-rF FILENAME]
                        [-e ENVIROMENT] [-t CPUs] [-gF FILENAME] [-rs INT]
                        [-rm] [-sd FLOAT] [-tx FILENAME]
                        input bed
```

##### 2.1.1.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | batch_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | batch_output | Output directory name. Empty for default. |
| -T {cnvkit,panelcnMOPS,both}, --tools {cnvkit,panelcnMOPS,both}| both | Tools to use in combination with DECoN in order to obtain the validation dataset. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -pl, --plot | - | Create an automated visualization (plot) for all variants. |
| -rS, --rmSample | - | Remove samples that do no pass the mincorr and the mincov threshold. |
| -rR, --rmRegion | - | Remove regions that do no pass the mincorr and the mincov threshold. |
| -pD DIRECTORY, --DECoNPath DIRECTORY | ~/programs/DECoN-1.0.2/Linux | Path to the folder containing DECoN program. |
| -pO DIRECTORY, --optimizerPath DIRECTORY | ~/programs/CNVbenchmarkeR/optimizers | Path to the folder containing optimizer program. |
| -pA DIRECTORY, --AnnotSVPath DIRECTORY | ~/programs/AnnotSV/bin | Path to the folder containing AnnotSV program. |
| -rF FILENAME, --relationshipFile FILENAME | - | Relationship file. Each line of the file should correspond to a family and each column of the line (separated by tab) should correspond to a family member. The sample name of each family member should correspond with the name of the BAM file. For example: if the BAM file is sample1_sorted.bam, the sample name should be sample1_sorted. |
| -e ENVIROMENT, --env ENVIROMENT | cnvkit_final | Conda enviroment where conda is installed. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -gF FILENAME, --genderFile FILENAME | - | Tab file with the samples gender. The first column should contain the samplename and the second column should contain the gender (female or male). Empty for infer gender of the samples using gender tool. |
| -rs INT, --read-size INT | 150 | Read length. |
| -rm, --remove | - | Remove intermediate files and directories. |
| -sd FLOAT, --max-sd FLOAT | 2 | Maximum standard deviation allowed for a sample to be used as reference in cnvkit. |
| -tx FILENAME, --txFile FILENAME | - | Path of a file containing a list of preferred genes transcripts to be used in priority during the annotation (Preferred genes transcripts names should be tab or space separated) |

##### 2.1.1.3.- Outputs

- Raw CNVs. Default: batch_output.txt. Columns of the file:
	- Chromosome
	- Start
	- End
	- Copy Number Variant type (DEL or DUP)
	- Sample
	- Reads ratio 
	- Precise copy number value
- Annotated CNVs. Default: batch_output_annotation.txt.
- Inferred gender of the samples (if -gF or --genderFile is not given). Default: batch_output_gender.txt.
- Failured regions. Default: batch_output_Failures_final.txt.
- Failured regions - Gene centric. Default: batch_output_Failures_final_geneCentric.txt.
- Folder with the automated visualization plots created by DECoN (if -pl or --plot is chosen). Default: Plots.


#### 2.1.2.- Batch 2

##### 2.1.2.1. - Usage
```python
usage: isoCNV.py batch2 [-h] -f FASTA [-o FILENAME] [-O DIRECTORY] [-T TOOL]
                        [-g GENOME] [-pl] [-rS] [-rR] [-pD DIRECTORY]
                        [-pO DIRECTORY] [-pA DIRECTORY] [-rF FILENAME]
                        [-e ENVIROMENT] [-t CPUs] [-gF FILENAME] [-rs INT]
                        [-rm] [-sd FLOAT] [-tx FILENAME]
                        input bed
```

##### 2.1.2.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | batch2_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | batch2_output | Output directory name. Empty for default. |
| -T {cnvkit,panelcnMOPS,both}, --tools {cnvkit,panelcnMOPS,both}| both | Tools to use in combination with DECoN in order to obtain the validation dataset. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -pl, --plot | - | Create an automated visualization (plot) for all variants. |
| -rS, --rmSample | - | Remove samples that do no pass the mincorr and the mincov threshold. |
| -rR, --rmRegion | - | Remove regions that do no pass the mincorr and the mincov threshold. |
| -pD DIRECTORY, --DECoNPath DIRECTORY | ~/programs/DECoN-1.0.2/Linux | Path to the folder containing DECoN program. |
| -pO DIRECTORY, --optimizerPath DIRECTORY | ~/programs/CNVbenchmarkeR/optimizers | Path to the folder containing optimizer program. |
| -pA DIRECTORY, --AnnotSVPath DIRECTORY | ~/programs/AnnotSV/bin | Path to the folder containing AnnotSV program. |
| -rF FILENAME, --relationshipFile FILENAME | - | Relationship file. Each line of the file should correspond to a family and each column of the line (separated by tab) should correspond to a family member. The sample name of each family member should correspond with the name of the BAM file. For example: if the BAM file is sample1_sorted.bam, the sample name should be sample1_sorted. |
| -e ENVIROMENT, --env ENVIROMENT | cnvkit_final | Conda enviroment where conda is installed. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -gF FILENAME, --genderFile FILENAME | - | Tab file with the samples gender. The first column should contain the samplename and the second column should contain the gender (female or male). Empty for infer gender of the samples using gender tool. |
| -rs INT, --read-size INT | 150 | Read length. |
| -rm, --remove | - | Remove intermediate files and directories. |
| -sd FLOAT, --max-sd FLOAT | 2 | Maximum standard deviation allowed for a sample to be used as reference in cnvkit. |
| -tx FILENAME, --txFile FILENAME | - | Path of a file containing a list of preferred genes transcripts to be used in priority during the annotation (Preferred genes transcripts names should be tab or space separated) |

##### 2.1.2.3.- Outputs

- Raw CNVs. Default: batch2_output.txt. Columns of the file:
	- Chromosome
	- Start
	- End
	- Copy Number Variant type (DEL or DUP)
	- Sample
	- Reads ratio 
	- Precise copy number value
- Annotated CNVs. Default: batch2_output_annotation.txt.
- Inferred gender of the samples (if -gF or --genderFile is not given). Default: batch_output_gender.txt.
- Failured regions. Default: batch2_output_Failures_final.txt.
- Failured regions - Gene centric. Default: batch2_output_Failures_final_geneCentric.txt.
- Folder with the automated visualization plots created by DECoN (if -pl or --plot is chosen). Default: Plots.

### 2.2.- Step-by-step analysis

#### 2.2.1.- Obtain gender of the samples

##### 2.2.1.1.- Usage

```python
usage: isoCNV.py gender [-h] [-O DIRECTORY] [-g GENOME] [-e ENVIROMENT]
                         [-t CPUs]
                         input bed
```
##### 2.2.1.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -O DIRECTORY, --output-dir DIRECTORY | gender_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -e ENVIROMENT, --env ENVIROMENT | cnvkit_final | Conda enviroment where conda is installed. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |

#### 2.2.2.- DECoN

##### 2.2.2.1.- Usage

```python
usage: isoCNV.py DECoN [-h] -f FASTA [-o FILENAME] [-O DIRECTORY] [-g GENOME]
                        [-mcr FLOAT] [-mcv FLOAT] [-tp FLOAT] [-pl] [-rS]
                        [-rR] [-p DIRECTORY] [-rF FILENAME]
                        input bed


```
##### 2.2.2.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | DECoN_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | DECoN_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -mcr FLOAT, --mincorr FLOAT | 0.98 | Minimum correlation threshold. The minimum correlation between a test sample and any other sample for the test sample to be considered well correlated. |
| -mcv FLOAT, --mincov FLOAT | 100 | Minimum coverage threshold. The minimum median coverage for any sample (measured across all exons in the target) or exon (measured across all samples) to be considered well-covered. |
| -tp FLOAT, --transProb FLOAT | 0.01 | Transition probability. The transition probability between normal copy number state and either deletion or duplication state in the hidden Markov model. |
| -pl, --plot | - | Create an automated visualization (plot) for all variants. |
| -rS, --rmSample | - | Remove samples that do no pass the mincorr and the mincov threshold. |
| -rR, --rmRegion | - | Remove regions that do no pass the mincorr and the mincov threshold. |
| -p DIRECTORY, --toolPath DIRECTORY | ~/programs/DECoN-1.0.2/Linux | Path to the folder containing DECoN program. |
| -rF FILENAME, --relationshipFile FILENAME | - | Relationship file. Each line of the file should correspond to a family and each column of the line (separated by tab) should correspond to a family member. The sample name of each family member should correspond with the name of the BAM file. For example: if the BAM file is sample1_sorted.bam, the sample name should be sample1_sorted. |

#### 2.2.3.- CNVkit

##### 2.2.3.1.- Usage

```python
usage: isoCNV.py cnvkit [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                         [-g GENOME] [-e ENVIROMENT] [-r REFERENCES]
                         [-sd FLOAT] [-t CPUs] [-gF FILENAME] [-m SEQUENCING]
                         input bed
```

##### 2.2.3.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | cnvkit_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | cnvkit_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -e ENVIROMENT, --env ENVIROMENT | cnvkit_final | Conda enviroment where conda is installed. |
| -r REFERENCES, --references REFERENCES | gender_output/samples_females.txt | Path to a text file containing the samples to be used as reference. Each line should correspond to a sample. In order to be able to call CNV in chrX, only female samples should be uses as references. |
| -sd FLOAT, --max-sd FLOAT | 2 | Maximum standard deviation allowed for a sample to be used as reference. |
| -t CPUs, --threads CPUs | Maximum available at your computer | Number of CPUs. |
| -gF FILENAME, --genderFile FILENAME | - | Tab file with the samples gender. The first column should contain the samplename and the second column should contain the gender (female or male). Empty for infer gender of the samples using gender tool. |
| -m {amplicon,wgs}, --method {amplicon,wgs} | amplicon | Sequencing method. |

#### 2.2.4.- panelcnMOPS

##### 2.2.4.1.- Usage

```python
usage: isoCNV.py panelcnMOPS [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                              [-g GENOME] [-rs INT] [-rR] [--gender GENDER]
                              input bed
```

##### 2.2.4.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | -   | show this help message and exit. |
| -o FILENAME, --output FILENAME | panelcnMOPS_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | panelcnMOPS_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -rs INT, --read-size INT | 150 | Read length. |
| -rR, --rmRegion | - | Remove regions that do no pass the mincorr and the mincov threshold. |
| --gender {female,male,mixed} | mixed | Gender of the input samples. |

#### 2.2.5.- Merge individual call sets

##### 2.2.5.1.- Usage

```python
usage: isoCNV.py merge [-h] [-o FILENAME] [-O DIRECTORY] [-g GENOME] [-c BED]
                        [--min-overlap FLOAT]
                        a_bed_calls b_bed_calls bed
```

##### 2.2.5.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| a_bed_calls | -   | PATH to BED file. The BED file should be contain the CNV calls. The BED file should not have header. |
| b_bed_calls | -   | PATH to BED file. The BED file should be contain the CNV calls. The BED file should not have header. |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | - | show this help message and exit. |
| -o FILENAME, --output FILENAME | merge_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | merge_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -c BED, --c-bed-calls BED | - | Complete PATH to BED file. The BED file should be contain the CNV calls. The BED file should not have header. |
| --min-overlap FLOAT  | 0.6 | Minimum overlap required betweeen the call sets from the algorithms. |

#### 2.2.6.- Optimization

##### 2.2.6.1.- Usage

```python
usage: isoCNV.py optimizer [-h] -f FASTA [-o FILENAME] [-O DIRECTORY]
                            [-g GENOME] [-p DIRECTORY] [-pD DIRECTORY]
                            [-rs INT]
                            input bed bed_real_calls
```

##### 2.2.6.2.- Arguments

###### Mandatory Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| input | -   | PATH to text file. The text file must contain a list of the BAM files to be read in, with each file name on a separate line.   |
| bed | - | PATH to the BED file. The BED file should be the baited genomic regions for your target capture kit. The BED file should not have header. |
| bed_real_calls | - | PATH to BED file (output of merge tool). The BED file should contain the real CNV calls. The BED file should not have header. |
| -f FASTA, --fasta FASTA | - | Reference genome FASTA file to be used. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | - | show this help message and exit. |
| -o FILENAME, --output FILENAME | optimizer_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | optimizer_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -p DIRECTORY, --toolPath DIRECTORY | ~/programs/CNVbenchmarkeR/optimizers | Path to the folder containing optimizer program. |
| -pD DIRECTORY, --DECoNPath DIRECTORY | ~/programs/DECoN-1.0.2/Linux | Path to the folder containing DECoN program. |
| -rs INT, --read-size INT | 150 | Read length. |

#### 2.2.7.- Annotation

##### 2.2.7.1.- Usage

```python
usage: isoCNV.py annotation [-h] [-o FILENAME] [-O DIRECTORY] [-g GENOME]
                             [-tx FILENAME] [-p DIRECTORY]
                             bed_calls
```

##### 2.2.7.2.- Arguments

###### Positional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| bed_calls | - | PATH to BED file. The BED file should be contain the CNV calls. The BED file should not have header. |

###### Optional Arguments

| Parameter | Default value | Explanation |
| :---:       |  :---:   |:---  |
| -h, --help   | - | show this help message and exit. |
| -o FILENAME, --output FILENAME | annotation_output | Output filename. Empty for default. |
| -O DIRECTORY, --output-dir DIRECTORY | annotation_output | Output directory name. Empty for default. |
| -g {hg19,hg38}, --genome {hg19,hg38} | hg19 | Genome assembly. |
| -tx FILENAME, --txFile FILENAME | - | Path of a file containing a list of preferred genes transcripts to be used in priority during the annotation (Preferred genes transcripts names should be tab or space separated) |
| -p DIRECTORY, --toolPath DIRECTORY | ~/programs/AnnotSV/bin | Path to the folder containing AnnotSV program. |

