#!/usr/bin/env python3
"""Command-line interface for calling Copy Number Variation."""
import sourcedefender
import logging
from cnvlib import commands

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(message)s")
    args = commands.parse_args()
    args.func(args)
