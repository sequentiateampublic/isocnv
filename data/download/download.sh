# ******************************************
# 1. Download hg19 cannonical transcript annotation
#https://genome.ucsc.edu/cgi-bin/hgTables?hgsid=913545067_ewpMlvsRCXodirD54t3tlYXrek2q&clade=mammal&org=Human&db=hg19&hgta_group=genes&hgta_track=refSeqComposite&hgta_table=ncbiRefSeqSelect&hgta_regionType=genome&position=chrX%3A15%2C578%2C261-15%2C621%2C068&hgta_outputType=selectedFields&hgta_outFileName=test.txt
# ******************************************

# 1.- Download a bed file for the canonical transcripts using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select (ncbiRefSeqSelect)
# output format: select fields from primary and related tables
# press get output
# select fields from hg19.knownCanonical: name,chrom, txStart, txEnd,exonStarts, exonEnds,name2
# press get output

head  RefSeq_Select_transcripts.bed

# #name	chrom	txStart	txEnd	exonStarts	exonEnds	name2
# NM_032291.4	chr1	66999835	67216822	66999835,67091529,67098752,67101626,67105459,67108492,67109226,67126195,67133212,67136677,67137626,67138963,67142686,67145360,67147551,67154830,67155872,67161116,67184976,67194946,67199430,67205017,67206340,67206954,67208755,	67000051,67091593,67098777,67101698,67105516,67108547,67109402,67126207,67133224,67136702,67137678,67139049,67142779,67145435,67148052,67154958,67155999,67161176,67185088,67195102,67199563,67205220,67206405,67207119,67216822,	SGIP1
# NM_001080397.2	chr1	8378144	8404227	8378144,8384365,8385357,8385877,8390268,8395496,8397875,8399552,8403806,	8378246,8384786,8385450,8386102,8390996,8395650,8398052,8399758,8404227,	SLC45A1
# NM_018090.5	chr1	16767218	16786573	16767218,16770126,16774364,16774554,16775587,16778332,16782312,16785336,	16767348,16770227,16774469,16774636,16775696,16778510,16782388,16786573,	NECAP2


# 2.- Download a bed file for all UCSC exons using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select (ncbiRefSeqSelect)
#output format: BED - browser extensible data
#press get output
#select option Exons
#press get BED


head RefSeq_Select_exons.bed
# chr1	66999835	67000051	NM_032291.4_exon_0_0_chr1_66999836_f	0	+
# chr1	67091529	67091593	NM_032291.4_exon_1_0_chr1_67091530_f	0	+
# chr1	67098752	67098777	NM_032291.4_exon_2_0_chr1_67098753_f	0	+
# chr1	67101626	67101698	NM_032291.4_exon_3_0_chr1_67101627_f	0	+
# chr1	67105459	67105516	NM_032291.4_exon_4_0_chr1_67105460_f	0	+
# chr1	67108492	67108547	NM_032291.4_exon_5_0_chr1_67108493_f	0	+
# chr1	67109226	67109402	NM_032291.4_exon_6_0_chr1_67109227_f	0	+
# chr1	67126195	67126207	NM_032291.4_exon_7_0_chr1_67126196_f	0	+

# 3.- Download a bed file for all UCSC introns using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select (ncbiRefSeqSelect)
#output format: BED - browser extensible data
#press get output
#select option Introns
#press get BED


head RefSeq_Select_introns.bed
# chr1	67000051	67091529	NM_032291.4_intron_0_0_chr1_67000052_f	0	+
# chr1	67091593	67098752	NM_032291.4_intron_1_0_chr1_67091594_f	0	+
# chr1	67098777	67101626	NM_032291.4_intron_2_0_chr1_67098778_f	0	+
# chr1	67101698	67105459	NM_032291.4_intron_3_0_chr1_67101699_f	0	+
# chr1	67105516	67108492	NM_032291.4_intron_4_0_chr1_67105517_f	0	+


# 4.- Concatenate exons, introns:
rm RefSeq_Select_all.bed

cat RefSeq_Select_exons.bed RefSeq_Select_introns.bed >> RefSeq_Select_all.bed

# 5.- Modify the file to separate the transcript name of the rest of information:

awk '{split ($4,a,"_"); {print $1"\t"$2"\t"$3"\t"a[1]"_"a[2]"\t"a[3]"_"a[4]"\t"$6}}' RefSeq_Select_all.bed > RefSeq_Select_all_modif.bed

head RefSeq_Select_all_modif.bed
# chr1	66999835	67000051	NM_032291.4	exon_0	+
# chr1	67091529	67091593	NM_032291.4	exon_1	+
# chr1	67098752	67098777	NM_032291.4	exon_2	+
# chr1	67101626	67101698	NM_032291.4	exon_3	+



# 8.-Change the transcript identificator in UCSC_exons_modif.bed by the gene name if exits in the list of canonical trasncripts:

python /media/sequentia/synology_office/Rosa/CNV_pipelines/WES_TGS/CNVcall/data/download/trancriptTOgene_refGene.py RefSeq_Select_transcripts.bed RefSeq_Select_all_modif.bed > hg19_RefSeqSelect.tmp.bed

#Keep only "normals" chromosomes
awk 'BEGIN{FS="\t";OFS="\t"}{if($1 !~ "_"){print}}' hg19_RefSeqSelect.tmp.bed > hg19_RefSeqSelect.tmp2.bed

#Sort BED 
bedtools sort -i hg19_RefSeqSelect.tmp2.bed > hg19_RefSeqSelect.bed



# ******************************************
# 1. Download hg38 cannonical transcript annotation
#https://genome.ucsc.edu/cgi-bin/hgTables?hgsid=913545067_ewpMlvsRCXodirD54t3tlYXrek2q&clade=mammal&org=Human&db=hg38&hgta_group=genes&hgta_track=refSeqComposite&hgta_table=ncbiRefSeqSelect&hgta_regionType=genome&position=chrX%3A15%2C578%2C261-15%2C621%2C068&hgta_outputType=selectedFields&hgta_outFileName=test.txt
# ******************************************

# 1.- Download a bed file for the canonical transcripts using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select+MANE (ncbiRefSeqSelect)
# output format: select fields from primary and related tables
# press get output
# select fields from hg38.knownCanonical: name,chrom, txStart, txEnd,exonStarts, exonEnds,name2
# press get output

head  RefSeq_Select_transcripts.bed

# #name	chrom	txStart	txEnd	exonStarts	exonEnds	name2
# NM_032291.4	chr1	66999835	67216822	66999835,67091529,67098752,67101626,67105459,67108492,67109226,67126195,67133212,67136677,67137626,67138963,67142686,67145360,67147551,67154830,67155872,67161116,67184976,67194946,67199430,67205017,67206340,67206954,67208755,	67000051,67091593,67098777,67101698,67105516,67108547,67109402,67126207,67133224,67136702,67137678,67139049,67142779,67145435,67148052,67154958,67155999,67161176,67185088,67195102,67199563,67205220,67206405,67207119,67216822,	SGIP1
# NM_001080397.2	chr1	8378144	8404227	8378144,8384365,8385357,8385877,8390268,8395496,8397875,8399552,8403806,	8378246,8384786,8385450,8386102,8390996,8395650,8398052,8399758,8404227,	SLC45A1
# NM_018090.5	chr1	16767218	16786573	16767218,16770126,16774364,16774554,16775587,16778332,16782312,16785336,	16767348,16770227,16774469,16774636,16775696,16778510,16782388,16786573,	NECAP2


# 2.- Download a bed file for all UCSC exons using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select+MANE  (ncbiRefSeqSelect)
#output format: BED - browser extensible data
#press get output
#select option Exons
#press get BED


head RefSeq_Select_exons.bed
# chr1	66999835	67000051	NM_032291.4_exon_0_0_chr1_66999836_f	0	+
# chr1	67091529	67091593	NM_032291.4_exon_1_0_chr1_67091530_f	0	+
# chr1	67098752	67098777	NM_032291.4_exon_2_0_chr1_67098753_f	0	+
# chr1	67101626	67101698	NM_032291.4_exon_3_0_chr1_67101627_f	0	+
# chr1	67105459	67105516	NM_032291.4_exon_4_0_chr1_67105460_f	0	+
# chr1	67108492	67108547	NM_032291.4_exon_5_0_chr1_67108493_f	0	+
# chr1	67109226	67109402	NM_032291.4_exon_6_0_chr1_67109227_f	0	+
# chr1	67126195	67126207	NM_032291.4_exon_7_0_chr1_67126196_f	0	+

# 3.- Download a bed file for all UCSC introns using UCSC Table Browser:

# track: NCBI RefSeq
# table: RefSeq Select+MANE  (ncbiRefSeqSelect)
#output format: BED - browser extensible data
#press get output
#select option Introns
#press get BED


head RefSeq_Select_introns.bed
# chr1	67000051	67091529	NM_032291.4_intron_0_0_chr1_67000052_f	0	+
# chr1	67091593	67098752	NM_032291.4_intron_1_0_chr1_67091594_f	0	+
# chr1	67098777	67101626	NM_032291.4_intron_2_0_chr1_67098778_f	0	+
# chr1	67101698	67105459	NM_032291.4_intron_3_0_chr1_67101699_f	0	+
# chr1	67105516	67108492	NM_032291.4_intron_4_0_chr1_67105517_f	0	+


# 4.- Concatenate exons, introns:
rm RefSeq_Select_all.bed

cat RefSeq_Select_exons.bed RefSeq_Select_introns.bed >> RefSeq_Select_all.bed

# 5.- Modify the file to separate the transcript name of the rest of information:

awk '{split ($4,a,"_"); {print $1"\t"$2"\t"$3"\t"a[1]"_"a[2]"\t"a[3]"_"a[4]"\t"$6}}' RefSeq_Select_all.bed > RefSeq_Select_all_modif.bed

head RefSeq_Select_all_modif.bed
# chr1	66999835	67000051	NM_032291.4	exon_0	+
# chr1	67091529	67091593	NM_032291.4	exon_1	+
# chr1	67098752	67098777	NM_032291.4	exon_2	+
# chr1	67101626	67101698	NM_032291.4	exon_3	+



# 8.-Change the transcript identificator in UCSC_exons_modif.bed by the gene name if exits in the list of canonical trasncripts:

python /media/sequentia/synology_office/Rosa/CNV_pipelines/WES_TGS/CNVcall/data/download/trancriptTOgene_refGene.py RefSeq_Select_transcripts.bed RefSeq_Select_all_modif.bed > hg19_RefSeqSelect.tmp.bed

#Keep only "normals" chromosomes
awk 'BEGIN{FS="\t";OFS="\t"}{if($1 !~ "_"){print}}' hg19_RefSeqSelect.tmp.bed > hg19_RefSeqSelect.tmp2.bed

#Sort BED 
bedtools sort -i hg19_RefSeqSelect.tmp2.bed > hg19_RefSeqSelect.bed


#Remove chr prefix
awk 'BEGIN{FS="\t";OFS="\t"}{gsub("chr","",$1); print}' hg19_RefSeqSelect.bed > hg19_RefSeqSelect_noPrefix.bed