#!/usr/bin/env python

import csv
import sys
import re


input_file = csv.reader(open (sys.argv[1], "r"), delimiter="\t")
input_file2 = csv.reader(open (sys.argv[2], "r"), delimiter="\t")


Chr_dict = dict()
for line in input_file:
    Chr_dict[line[1]] = line[0]



for line2 in input_file2:
    if line2[0].startswith("#"):
        print "\t".join(line2)
    else:
        if line2[0] in Chr_dict.keys():
            line2[0]=Chr_dict[line2[0]]
            print "\t".join(line2)
        else:
            print "No chromosome nomenclature found"
            sys.exit()
