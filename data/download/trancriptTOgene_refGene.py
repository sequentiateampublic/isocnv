#!/usr/bin/env python

import csv
import sys
import re


input_file = csv.reader(open (sys.argv[1], "r"), delimiter="\t")
input_file2 = csv.reader(open (sys.argv[2], "r"), delimiter="\t")

GENES_dict = dict()
for line in input_file:
    GENES_dict[line[0]] = line[6]

for line2 in input_file2:
    if line2[3] in GENES_dict.keys():
        newline = [line2[0],line2[1],line2[2], GENES_dict[line2[3]],line2[4],line2[5]]
        print ("\t".join(newline))




